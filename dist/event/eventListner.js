"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = require("../connection/socket");
const eventEmmiter_1 = __importDefault(require("./eventEmmiter"));
const eventName_1 = require("../constants/eventName");
const startListner = () => {
    eventEmmiter_1.default.on(eventName_1.Event.SIGNUP, (data, socket) => {
        socket_1.io.sockets.to(socket.id).emit(eventName_1.Event.SIGNUP, data);
    });
    eventEmmiter_1.default.on(eventName_1.Event.LOGIN, (data, socket) => {
        socket_1.io.sockets.to(socket.id).emit(eventName_1.Event.SIGNUP, data);
    });
};
exports.default = startListner;
