"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketConnection = exports.io = void 0;
const socket_io_1 = require("socket.io");
const app_1 = require("../app");
const user_1 = __importDefault(require("../model/user"));
const handler_1 = require("../handler");
exports.io = new socket_io_1.Server(app_1.app, {
    cors: {
        origin: "*"
    }
});
function SocketConnection() {
    return __awaiter(this, void 0, void 0, function* () {
        exports.io.on('connection', function (socket) {
            return __awaiter(this, void 0, void 0, function* () {
                console.log(socket.id, 'socket.id');
                console.log(exports.io.sockets.adapter.rooms);
                socket.onAny((eventName, ...args) => __awaiter(this, void 0, void 0, function* () {
                    yield (0, handler_1.requestHandler)(eventName, args[0], socket);
                }));
                socket.on('disconnect', () => __awaiter(this, void 0, void 0, function* () {
                    console.log('DisconnectTD', socket.id);
                    const user = yield user_1.default.findOne({ socketID: socket.id });
                    console.log('User', user);
                    if (user) {
                        yield user_1.default.findOneAndUpdate({ socketID: socket.id }, {
                            $set: {
                                status: 'offline'
                            }
                        }, { new: true, upsert: true });
                    }
                }));
                // socket.on('disconnect',()=>{
                //     console.log('DisconnectTD', socket.userID);
                //     const user = await User.findById({_id: socket.userID});
                //     console.log('User', user);
                //     if(user){
                //         user = await User.findByIdAndUpdate({_id: socket.userID}, {
                //             $set: {
                //                 status: 'offline'
                //             }
                //         }, { new: true, upsert: true });
                //     }
                // })
            });
        });
    });
}
exports.SocketConnection = SocketConnection;
