"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dbConnect = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const config_1 = require("../config");
const MONGO_URI = config_1.config.MONGO_URI;
function dbConnect() {
    mongoose_1.default.connect(MONGO_URI, {}).then(() => {
        console.log("Successfully connected to database");
    }).catch((error) => {
        console.error(error);
        console.log("Database connection field.");
        process.exit(1);
    });
}
exports.dbConnect = dbConnect;
;
