"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const mongoose_1 = __importDefault(require("mongoose"));
const messageSchema = new mongoose_1.default.Schema({
    sender: { type: mongoose_1.default.Schema.Types.ObjectId },
    message: { type: String }
});
const RoomSchema = new mongoose_1.default.Schema({
    room_name: {
        type: String
    },
    users: [{
            type: mongoose_1.default.Schema.Types.ObjectId,
            ref: 'User'
        }],
    message: [messageSchema],
    createdBy: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'User'
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});
const Room = mongoose_1.default.model('room', RoomSchema);
module.exports = Room;
