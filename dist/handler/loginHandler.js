"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.login = void 0;
const user_1 = __importDefault(require("../model/user"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = require("../config");
const eventName_1 = require("../constants/eventName");
function login(data, socket) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let user = yield user_1.default.findOne({ email: data.email });
            console.log('User', user);
            if (user) {
                user = yield user_1.default.findOneAndUpdate({ email: data.email }, {
                    $set: {
                        socketID: socket.id,
                        status: 'online'
                    }
                }, { new: true });
                if (!user)
                    return socket.emit(eventName_1.Event.LOGIN, { message: `Incorrect Email and Password`, code: 400, error: false, data: {} });
                const validatePassword = yield bcrypt_1.default.compare(data.password, user.password);
                let payload = {
                    _id: user._id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                    socketID: user.socketID,
                    status: user.status,
                };
                if (validatePassword) {
                    const token = jsonwebtoken_1.default.sign({ user_id: user.id, email: data.email, socketID: socket.id }, config_1.config.SECRET_KEY);
                    payload.token = token;
                    //const userID = verifyToken(token);
                    console.log('User:-', user.id);
                    const sender = user.id;
                    // socket data save
                    console.log('SenderID:-', sender);
                    return socket.emit(eventName_1.Event.LOGIN, { message: `Login SuccessFully`, code: 200, error: false, data: { payload } });
                }
                else {
                    return socket.emit(eventName_1.Event.LOGIN, { message: 'Incorrect Email and Password', code: 400, error: true, data: {} });
                }
            }
            else {
                console.log("not found");
            }
        }
        catch (error) {
            return socket.emit(eventName_1.Event.LOGIN, { message: error, code: 509, error: false, data: {} });
        }
    });
}
exports.login = login;
