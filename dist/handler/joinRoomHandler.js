"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.joinroom = void 0;
const room_1 = __importDefault(require("../model/room"));
const eventName_1 = require("../constants/eventName");
const auth_1 = __importDefault(require("../middleware/auth"));
function joinroom(data, socket) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const user = (0, auth_1.default)(data === null || data === void 0 ? void 0 : data.token);
            // @ts-ignore
            const sender = user.user_id;
            console.log('SenderID', sender);
            let RoomFind;
            if (data.roomID) {
                console.log(data.roomID);
                RoomFind = yield room_1.default.findById({ _id: data.roomID });
                console.log(RoomFind, "FindRoom");
            }
            if (!RoomFind) {
                const roomdata = new room_1.default({
                    room_name: data.room_name,
                    users: sender,
                    createdBy: sender
                });
                RoomFind = yield roomdata.save();
                return socket.emit(eventName_1.Event.JOINROOM, { message: 'Room Created!', code: 200, error: false, data: { RoomFind } });
            }
            else {
                const userdata = RoomFind.users;
                console.log('Users Array', userdata);
                let user = false;
                userdata.forEach((element, index) => {
                    console.log(element, index);
                    if (element == sender) {
                        user = true;
                        console.log('In Room');
                    }
                });
                if (user == false) {
                    RoomFind = yield room_1.default.findByIdAndUpdate({ _id: data.roomID }, {
                        $push: {
                            users: sender
                        }
                    }, { new: true, upsert: true });
                    console.log('Added In Room');
                }
            }
            const room = RoomFind._id.toString();
            console.log("ROOM", room);
            socket.join(room);
            socket.to(room).emit('res', RoomFind);
            return socket.emit(eventName_1.Event.JOINROOM, { message: 'join Room SuccessFully', code: 200, error: false, data: room });
        }
        catch (error) {
            return socket.emit(eventName_1.Event.JOINROOM, { message: error, code: 509, error: true, data: {} });
        }
    });
}
exports.joinroom = joinroom;
