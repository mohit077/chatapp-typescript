"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.readby = void 0;
const room_1 = __importDefault(require("../model/room"));
const eventName_1 = require("../constants/eventName");
const auth_1 = __importDefault(require("../middleware/auth"));
function readby(data, socket) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const user = (0, auth_1.default)(data === null || data === void 0 ? void 0 : data.token);
            // @ts-ignore
            const sender = user.user_id;
            console.log('SenderID', sender);
            let room;
            room = yield room_1.default.find({ message: data.messageID, users: sender }).populate({ path: 'users', select: 'first_name status' });
            console.log(JSON.stringify(room));
            let data1 = room[0].users;
            console.log('Data1:', data1);
            let readBy = [];
            data1.forEach((element, index) => {
                let a = element.status;
                if (a = 'online') {
                    console.log('Online', a);
                    readBy.push(element.first_name);
                }
                else {
                    console.log('offline', a);
                }
            });
            return socket.emit(eventName_1.Event.READBY, { message: 'Read By...', code: 200, error: false, data: { readBy } });
        }
        catch (error) {
            return socket.emit(eventName_1.Event.READBY, { message: error, code: 509, error: true, data: {} });
        }
    });
}
exports.readby = readby;
