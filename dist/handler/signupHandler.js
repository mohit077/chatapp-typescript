"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.signup = void 0;
const user_1 = __importDefault(require("../model/user"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const eventName_1 = require("../constants/eventName");
function signup(data, socket) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const encryptedPassword = yield bcrypt_1.default.hash(data.password, 10);
            console.log('SocketID:', socket.id);
            const userdata = new user_1.default({
                first_name: data.first_name,
                last_name: data.last_name,
                email: data.email,
                password: encryptedPassword,
                socketID: socket.id,
                createdAt: data.createdAt,
                updatedAt: data.updatedAt
            });
            const user = yield userdata.save();
            console.log('DATA ADDED');
            return socket.emit(eventName_1.Event.SIGNUP, { message: `User Registred SuccessFully`, code: 200, error: false, data: { user } });
        }
        catch (error) {
            return socket.emit(eventName_1.Event.SIGNUP, { message: error.message, code: error.code, error: false, data: {} });
        }
    });
}
exports.signup = signup;
