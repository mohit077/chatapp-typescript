"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestHandler = void 0;
const joinRoomHandler_1 = require("./joinRoomHandler");
const signupHandler_1 = require("./signupHandler");
const loginHandler_1 = require("./loginHandler");
const sendMsgHandler_1 = require("./sendMsgHandler");
const ReadByHandler_1 = require("./ReadByHandler");
const eventName_1 = require("../constants/eventName");
function requestHandler(eventName, data, socket) {
    return __awaiter(this, void 0, void 0, function* () {
        switch (eventName) {
            case eventName_1.Event.SIGNUP:
                yield (0, signupHandler_1.signup)(data, socket);
                break;
            case eventName_1.Event.LOGIN:
                yield (0, loginHandler_1.login)(data, socket);
                break;
            case eventName_1.Event.JOINROOM:
                yield (0, joinRoomHandler_1.joinroom)(data, socket);
                break;
            case eventName_1.Event.SENDMSG:
                yield (0, sendMsgHandler_1.sendmsg)(data, socket);
                break;
            case eventName_1.Event.READBY:
                yield (0, ReadByHandler_1.readby)(data, socket);
                break;
        }
    });
}
exports.requestHandler = requestHandler;
