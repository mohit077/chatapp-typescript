"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendmsg = void 0;
const room_1 = __importDefault(require("../model/room"));
const eventName_1 = require("../constants/eventName");
const auth_1 = __importDefault(require("../middleware/auth"));
function readMsg(FindRoom) {
    console.log(JSON.stringify(FindRoom));
    const data1 = FindRoom.users;
    console.log('DATA', data1);
    let readBy = [];
    data1.forEach((element, index) => {
        const a = element.status;
        if (a == 'online') {
            readBy.push(element.firstname);
        }
    });
}
function sendmsg(data, socket) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const user = (0, auth_1.default)(data === null || data === void 0 ? void 0 : data.token);
            // @ts-ignore
            const sender = user.user_id;
            console.log('SenderID', sender);
            let FindRoom;
            FindRoom = yield room_1.default.findOne({ _id: data.roomID, users: sender }).populate({ path: 'users', select: 'first_name status' });
            console.log('Room', FindRoom);
            if (FindRoom) {
                const roomData = yield room_1.default.findByIdAndUpdate({ _id: FindRoom._id }, {
                    $push: {
                        message: [{
                                sender: sender,
                                message: data.message
                            }]
                    }
                }, { new: true, upsert: true }).populate({ path: 'users', select: 'first_name' });
                const room = data.roomID.toString();
                console.log('Room', room);
                socket.emit('res', { data: data.message });
                socket.to(room).emit('res', { data: data.message });
                return socket.emit(eventName_1.Event.SENDMSG, { message: 'Message Sent..!!', code: 200, error: false, data: { roomData } });
                // const read = readMsg(FindRoom);
                // return socket.emit('readMsg',{ message: 'Read By...', code: 200, error: false, data: {read} });
            }
        }
        catch (error) {
            console.log(error, "errorrrrrrrr");
            return socket.emit(eventName_1.Event.SENDMSG, { message: 'Room Not Found', code: 509, error: true, data: {} });
        }
    });
}
exports.sendmsg = sendmsg;
