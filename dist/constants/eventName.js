"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Event = void 0;
exports.Event = {
    SIGNUP: 'SIGNUP',
    LOGIN: 'LOGIN',
    JOINROOM: 'JOINROOM',
    SENDMSG: 'SENDMSG',
    READBY: 'READBY'
};
