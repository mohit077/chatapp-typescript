"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = require("../config");
const verifyToken = function (token) {
    // const token = token.header('auth-token');
    // if(!token) return res.status(401).send('Access Denied');
    try {
        const verified = jsonwebtoken_1.default.verify(token, config_1.config.SECRET_KEY);
        const user = verified;
        return user;
    }
    catch (error) {
        // res.status(400).send('Invalid Token');
    }
};
module.exports = verifyToken;
