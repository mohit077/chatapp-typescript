"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("./connection/mongoose");
const socket_1 = require("./connection/socket");
const { app } = require('./app');
(0, mongoose_1.dbConnect)();
(0, socket_1.SocketConnection)();
const port = 3000;
app.listen(port, () => {
    console.log(`Server is running on ${port}`);
});
