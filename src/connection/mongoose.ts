import mongoose from 'mongoose'
import {config} from '../config'

const MONGO_URI = config.MONGO_URI

export function dbConnect () {
    mongoose.connect(MONGO_URI, {

    }).then(() => {
        console.log("Successfully connected to database");
    }).catch((error) => {
        console.error(error);
        console.log("Database connection field.");
        process.exit(1);
    });
};