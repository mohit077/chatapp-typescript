import { Server, Socket, ServerOptions } from 'socket.io';
import { app } from '../app';
import User  from '../model/user';
import { requestHandler } from '../handler';

export var io = new Server(app, {
    cors: {
        origin: "*"
    }
});

export async function SocketConnection () {
    io.on('connection', async function (socket: Socket) {
        console.log(socket.id, 'socket.id');
        console.log(io.sockets.adapter.rooms);
    
        socket.onAny(async (eventName, ...args) => {
            await requestHandler(eventName, args[0], socket)
        });

        socket.on('disconnect', async() => {
            console.log('DisconnectTD', socket.id);
            const user:any = await User.findOne({socketID: socket.id});
            console.log('User', user);
            if(user){
                await User.findOneAndUpdate({socketID: socket.id}, {
                    $set: {
                        status: 'offline'
                    }
                }, { new: true, upsert: true });
            }
        })
        // socket.on('disconnect',()=>{
        //     console.log('DisconnectTD', socket.userID);
        //     const user = await User.findById({_id: socket.userID});
        //     console.log('User', user);
        //     if(user){
        //         user = await User.findByIdAndUpdate({_id: socket.userID}, {
        //             $set: {
        //                 status: 'offline'
        //             }
        //         }, { new: true, upsert: true });
        //     }
        // })
    });
    
}



