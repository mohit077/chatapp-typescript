interface signupInterface {
    first_name: string,
    last_name: string,
    email: string,
    password: string,
    socketID: string,
    createdAt: Date,
    updatedAt : Date,
    status: string
}


interface loginInterface {
    email: string,
    password: string,
    error: boolean,
}
export { signupInterface, loginInterface }