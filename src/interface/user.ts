interface userRes{
    _id: string,
    first_name:string,
    last_name: string,
    email: string,
    password: string,
    socketID: string,
    createdAt: Date,
    updatedAt: Date,
    status: string,
    _v:number
}

export { userRes }