interface TokenRes {
    user_id: string,
    email: string,
    socketID: string
}

export {TokenRes}