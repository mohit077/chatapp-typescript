interface I_RESPONSE {
    message?: string,
    data: any,
    error: boolean,  
    code: number,
}

export { I_RESPONSE }