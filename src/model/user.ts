import mongoose, {Document} from 'mongoose';

interface I_USER extends Document{
    first_name:string,
    last_name: string,
    email: string,
    password: string,
    socketID: string,
    status: string,
}
const UserSchema = new mongoose.Schema({
    first_name: {
        type: String,
        default: null,
    },
    last_name: {
        type: String, 
        default: null,
    },
    email: {
        type: String,
        unique: true, 
    },
    password: {
        type: String,
    },
    socketID: {
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    },
    status: {
        type: String,
        default: 'offline'
    } 
});

const User = mongoose.model<I_USER>('User', UserSchema)
export = User