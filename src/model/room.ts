import mongoose, { Document } from "mongoose";

interface I_ROOM extends Document{
    room_name: string,
    users: Array<string>,
    message: Array<string>,
    createdBy: string
}

const messageSchema = new mongoose.Schema({
    sender:{type: mongoose.Schema.Types.ObjectId},
    message:{type:String}
})

const RoomSchema = new mongoose.Schema({

    room_name: { 
        type: String 
    },
    users: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    message: [messageSchema],
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

const Room = mongoose.model<I_ROOM | undefined>('room', RoomSchema)
export = Room