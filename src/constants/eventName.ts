export var Event = {
    SIGNUP: 'SIGNUP',
    LOGIN: 'LOGIN',
    JOINROOM: 'JOINROOM',
    SENDMSG: 'SENDMSG',
    READBY: 'READBY'
}