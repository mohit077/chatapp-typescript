import { dbConnect } from "./connection/mongoose";
import { SocketConnection } from "./connection/socket";
const {app} = require('./app');
dbConnect();
SocketConnection();
const port = 3000


app.listen(port, () => {
    console.log(`Server is running on ${port}`);
})

