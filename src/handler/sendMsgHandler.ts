import Room from "../model/room";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { config } from "../config";
import { Socket } from "socket.io";
import { io, SocketConnection } from "../connection/socket"
import { Event } from "../constants/eventName";
import verifyToken from "../middleware/auth";

function readMsg(FindRoom:any) {
    console.log(JSON.stringify(FindRoom));

    const data1 = FindRoom.users;
    console.log('DATA', data1);
    
    let readBy = []
    data1.forEach((element:any,index:number) => {
        const a = element.status;
        if(a == 'online') {
            readBy.push(element.firstname);
        }
    });
}

export async function sendmsg(data:any, socket:Socket) {
    try {
        const user = verifyToken(data?.token);
        // @ts-ignore
        const sender = user.user_id;
        console.log('SenderID', sender);

        let FindRoom;
        FindRoom = await Room.findOne({ _id: data.roomID, users: sender }).populate({ path:'users', select: 'first_name status' });
        console.log('Room', FindRoom);

        if(FindRoom) {
            const roomData = await Room.findByIdAndUpdate({ _id: FindRoom._id }, {
                $push: {
                    message: [{
                        sender: sender,
                        message: data.message
                    }]
                }
            }, { new: true, upsert: true }).populate({ path: 'users', select: 'first_name' });

            const room = data.roomID.toString();
            console.log('Room',room);
            socket.emit('res', { data:data.message });
            socket.to(room).emit('res', {data:data.message});

            return socket.emit(Event.SENDMSG,{ message: 'Message Sent..!!', code: 200, error: false, data: {roomData} });

            

            // const read = readMsg(FindRoom);
            // return socket.emit('readMsg',{ message: 'Read By...', code: 200, error: false, data: {read} });
        }
    } catch (error) {
        console.log(error, "errorrrrrrrr");
        return socket.emit(Event.SENDMSG,{ message: 'Room Not Found', code: 509, error: true, data: {} });
    }   
}

