import Room from "../model/room";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { config } from "../config";
import { Socket } from "socket.io";
import { io, SocketConnection } from "../connection/socket"
import { Event } from "../constants/eventName";
import verifyToken from "../middleware/auth";


export async function readby(data:any, socket: Socket) {
    try {
        const user = verifyToken(data?.token);
        // @ts-ignore
        const sender = user.user_id;
        console.log('SenderID', sender);

        let room;
        room = await Room.find({ message: data.messageID, users: sender }).populate({ path: 'users', select: 'first_name status' });
        console.log(JSON.stringify(room));

        let data1 = room[0].users;
        console.log('Data1:', data1);
        

        let readBy:Array<string> = []
        data1.forEach((element:any,index:number) => {
            let a = element.status;
            if(a = 'online') {   
                console.log('Online', a);
                readBy.push(element.first_name);
            } else {
                console.log('offline', a);
            }
        });
        return socket.emit(Event.READBY,{ message: 'Read By...', code: 200, error: false, data: { readBy } });
    } catch (error) {
        return socket.emit(Event.READBY,{ message: error, code: 509, error: true, data: {} })
    }
}