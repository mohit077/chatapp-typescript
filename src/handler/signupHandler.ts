import { signupInterface } from '../interface/auth';
import User from '../model/user';
import bcrypt from 'bcrypt';
import { Socket } from 'socket.io';
import { Event } from '../constants/eventName';
import { io,SocketConnection } from '../connection/socket';

export async function signup(data: signupInterface, socket:Socket){
    try {
        const encryptedPassword =  await bcrypt.hash(data.password, 10);
        console.log('SocketID:', socket.id);
        const userdata = new User({
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            password: encryptedPassword,
            socketID: socket.id,
            createdAt: data.createdAt,
            updatedAt: data.updatedAt
        });
    
        const user = await userdata.save();
        console.log('DATA ADDED');
        return socket.emit(Event.SIGNUP, { message: `User Registred SuccessFully`, code: 200, error: false, data: {user} });
     
    } catch (error:any) {
        return socket.emit(Event.SIGNUP,{ message: error.message, code: error.code, error: false, data: {} })
    }
}