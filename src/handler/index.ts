import { joinroom } from "./joinRoomHandler";
import { signup } from "./signupHandler";
import { login } from "./loginHandler";
import { sendmsg } from "./sendMsgHandler";
import { readby } from "./ReadByHandler";
import { I_RESPONSE } from "../interface/common";
import { Event } from "../constants/eventName";
import { Socket } from "socket.io";
import { LogTimings } from "concurrently";


export async function requestHandler(eventName: string, data:any, socket: Socket){
    
    switch(eventName) {
        case Event.SIGNUP:
            await signup(data, socket)
        break;
        case Event.LOGIN:
            await login(data, socket)
        break;
        case Event.JOINROOM:
            await joinroom(data, socket)
        break;
        case Event.SENDMSG:
            await sendmsg(data, socket)
        break;
        case Event.READBY:
            await readby(data, socket)
        break;
    }   

}