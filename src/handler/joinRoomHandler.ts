import Room from "../model/room";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { config } from "../config";
import { Socket } from "socket.io";
import { io, SocketConnection } from "../connection/socket"
import { Event } from "../constants/eventName";
import verifyToken from "../middleware/auth";



export async function joinroom(data: any, socket: Socket) {
    try {
        const user = verifyToken(data?.token);
        // @ts-ignore
        const sender = user.user_id;
        console.log('SenderID', sender);
        
        let RoomFind;
        if(data.roomID){
            console.log(data.roomID);
            RoomFind = await Room.findById({_id: data.roomID});
            console.log(RoomFind, "FindRoom");
        }

        if(!RoomFind) {
            const roomdata = new Room({
                room_name: data.room_name,
                users: sender,
                createdBy: sender
            });
            RoomFind = await roomdata.save();
            return socket.emit(Event.JOINROOM, { message: 'Room Created!', code: 200, error: false, data: {RoomFind} });

        }else {
            const userdata = RoomFind.users;
            console.log('Users Array', userdata);

            let user = false;
            userdata.forEach((element,index) => {
                console.log(element,index);
                if(element == sender){
                    user = true;
                    console.log('In Room');
                }
            });

            if(user == false) {
                RoomFind =  await Room.findByIdAndUpdate({_id: data.roomID}, {
                    $push: {
                        users: sender
                    }
                }, {new: true, upsert: true});
                console.log('Added In Room');
            }
        }

        const room =  RoomFind._id.toString();
        console.log("ROOM", room);

        socket.join(room);
        socket.to(room).emit('res',RoomFind);
        return socket.emit(Event.JOINROOM,{ message: 'join Room SuccessFully', code: 200, error: false, data: room });
    } catch (error) {
        return socket.emit(Event.JOINROOM,{ message: error, code: 509, error: true, data: {} })
    }
}