import { loginInterface } from "../interface/auth";
import User, { find } from "../model/user";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { config } from "../config";
import { I_RESPONSE } from "../interface/common";
import { Socket } from "socket.io";
import { io, SocketConnection } from "../connection/socket"
import { Event } from "../constants/eventName";
import { userRes } from "../interface/user";
import verifyToken from "../middleware/auth";

export async function login(data: loginInterface, socket:Socket) {
    try {
        let user = await User.findOne({email: data.email});
        console.log('User', user);

        if(user) {
         user = await User.findOneAndUpdate({email: data.email}, {
            $set: {
                    socketID: socket.id,
                    status: 'online'
                }
            }, {new : true})
            if(!user) return socket.emit(Event.LOGIN, { message: `Incorrect Email and Password`, code: 400, error: false, data: {} });

            const validatePassword = await bcrypt.compare(data.password, user.password)

            let payload: any = {
                _id: user._id,
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email,
                socketID: user.socketID,
                status: user.status,
            }

            if(validatePassword) {
                const token = jwt.sign(
                    {user_id: user.id, email: data.email, socketID: socket.id}, config.SECRET_KEY
                );

                payload.token = token

                //const userID = verifyToken(token);
                console.log('User:-', user.id);
                const sender = user.id
    
                // socket data save
                console.log('SenderID:-', sender);

                return socket.emit(Event.LOGIN, { message: `Login SuccessFully`, code: 200, error: false, data: {payload} })
            }
            else {
                return socket.emit(Event.LOGIN, {message: 'Incorrect Email and Password', code: 400, error: true, data: {}})
            }
        }else{
            console.log("not found")  
        }
        
    } catch (error) {
        return socket.emit(Event.LOGIN,{ message: error, code: 509, error: false, data: {} })
    }
}