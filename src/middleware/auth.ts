import jwt, { JwtPayload } from "jsonwebtoken";

import {config} from '../config'
import { TokenRes } from "../interface/token";


const verifyToken = function(token: string) {
    // const token = token.header('auth-token');
    // if(!token) return res.status(401).send('Access Denied');

    try {
        const verified = jwt.verify(token, config.SECRET_KEY);
        const user: TokenRes | string | JwtPayload = verified;  
        return user;
    } catch (error) {
        // res.status(400).send('Invalid Token');
    }
}   


export= verifyToken;